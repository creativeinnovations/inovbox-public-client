/* global app */

app.controller('WebAppCtrl', ['$scope', '$location', '$window', '$sce', 'SocketFactory', '$timeout', '$interval', function($scope, $location, $window, $sce, SocketFactory, $timeout, $interval) {

    var vm = this;

    vm.status = {};
    vm.playlist = {};
    vm.element = {};

    vm.settings = {};
    vm.systemInfos = {};

    vm.isAwake = true;

    var timeoutHandler;
    var intervalHandler;

    var current = 0;

    vm.timerMax = 100;
    vm.timerCurrent = 10;

    vm.isPage = function(page) {
        return $location.path() === page;
    };

    vm.goTo = function(url) {
        $location.url(url);
    };

    vm.trustHTML = function(html) {
        return $sce.trustAsHtml(html);
    };

    vm.trustSrc = function(src) {
        return $sce.trustAsResourceUrl(src);
    };

    $scope.configModal = false;

    $scope.saveConfig = function(data) {
        SocketFactory.emit('config:save', data);
    };

    SocketFactory.on('status', function(status) {
        SocketFactory.emit('message', "OK");
        vm.status = status;
        //console.log(new Date().toLocaleString() + ": Status received!", status);

        // Si aucune config présente
        if (!status.config) {
            $scope.configModal = true;
        }
        else {
            $scope.configModal = false;
        }

        // Si config présente mais authenticated == false
        if (!status.authenticated) {
            $scope.configModal = true;
            $scope.config = status.config;
        }
        else {
            $scope.configModal = false;
        }
    });

    SocketFactory.on('config:saved', function(data) {
        //console.log(new Date().toLocaleString() + ": " + data.message);
        $scope.configModal = false;
    });

    SocketFactory.on('playlist:sync', function(pl) {
        // on cache le loader
        $scope.loader = false;

        current = 0;

        vm.playlist = pl;

        next();
    });

    SocketFactory.on('playlist:received', function (data) {
        //console.log(new Date().toLocaleString() + ": " + data.message);
        $scope.configModal = false;
        // on affiche le loader
        $scope.loader = true;
        // on stoppe toute vidéo OMX si lancée
        if (vm.systemInfos.platform !== 'win32') {
            SocketFactory.emit('video:stop');
        }
        // on cache tout
        vm.element = {};
        // on arrete tout
        if (timeoutHandler) $timeout.cancel(timeoutHandler);
        if (intervalHandler) $interval.cancel(intervalHandler);
    });

    SocketFactory.on('device:settings', function(data) {
        vm.settings = data;
    });

    SocketFactory.on('settings:sync', function(data) {
        vm.settings = data;
    });

    SocketFactory.on('system:infos', function(data) {
        vm.systemInfos = data;
    });

    function next() {
        if(typeof vm.playlist.elements !== 'undefined') {
            vm.timerCurrent = vm.playlist.elements[current].duration;
            vm.timerMax = vm.playlist.elements[current].duration

            if (timeoutHandler) $timeout.cancel(timeoutHandler);
            if (intervalHandler) $interval.cancel(intervalHandler);

            // on attribue l'element en cours
            if (vm.systemInfos.platform !== 'win32') {
                if (getExt(vm.playlist.elements[current].filename) == 'mp4') {
                    vm.element = {};
                    SocketFactory.emit('video:play', vm.playlist.elements[current].filename);
                } else {
                    vm.element = vm.playlist.elements[current];
                }
            } else {
                vm.element = vm.playlist.elements[current];
            }

            timeoutHandler = $timeout(function() {

                if (vm.systemInfos.platform !== 'win32') {
                    if (getExt(vm.playlist.elements[current].filename) == 'mp4') {
                        SocketFactory.emit('video:stop');
                    }
                }

                if (current < vm.playlist.elements.length-1) {
                    current++;
                }
                else {
                    current = 0;
                }
                next();
            }, vm.playlist.elements[current].duration * 1000);

            intervalHandler = $interval(function() {
                vm.timerCurrent -= 1;
            }, 1000);
        }
    };

    vm.getExt = getExt;

    function getExt(f) {
        if (!f) return null;
        return f.split('.').pop().toLowerCase();
    }

}]);

app.controller('PlaylistCtrl', ['$scope', '$location', function($scope, $location) {}]);