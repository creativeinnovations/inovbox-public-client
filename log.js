var util = require('util');
var colors = require('colors');

var getTimestamp = function() {
    return '[' + new Date().toLocaleTimeString().grey + '] ';
};

var types = {
    debug: "DEBUG: ".grey,
    success: "SUCCESS: ".green,
    info: "INFO: ".cyan,
    warn: "WARN: ".yellow,
    error: "ERROR: ".red
};

var log = function(type, value) {
    if (console) {
        console.log(getTimestamp() + type + ((typeof value === 'object') ? util.inspect(value, { colors: true }) : value));
    }
};

module.exports = {
    /**
     * Fonction de log console
     */
    debug: function(value) {
        log(types.debug, value);
    },

    success: function(value) {
        log(types.success, value);
    },

    info: function(value) {
        log(types.info, value);
    },

    warn: function(value) {
        log(types.warn, value);
    },

    error: function(value) {
        log(types.error, value);
    },
};